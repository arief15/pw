import { Container, Row, Button, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { css } from 'glamor'
export const Ucapan = (props) => {
    let ucapan = css({
        textAlign: "center",
        width: "100%",
        padding: "8em",
        color: '#D0B577',
        '@media (max-width:900px)': {
            padding: "3em",
        },
        '& h2': {
            fontFamily: 'Playfair Display, serif',
            fontSize: '1.6em',
        },
        '& h1': {
            fontFamily: 'julietta_messieregular',
            fontSize: '6em'
        },
        '& p': {
            fontFamily: 'Quicksand, sans-serif',
            fontSize: '0.9em',
        }
    })
    let shadow = css({
        backgroundColor: 'white',
        '-webkit-box-shadow': '0px 0px 36px 5px rgba(0,0,0,0.38)',
        '-moz-box-shadow': '0px 0px 36px 5px rgba(0,0,0,0.38)',
        'box-shadow': '0px 0px 36px 5px rgba(0,0,0,0.38)',
        borderRadius: '15px',
        verticalAlign: 'middle'
    })
    let left = css({
        '@media (min-width:900px)': {
            left: '-15px'
        },
        overflow: "hidden",
        
    })
    let right = css({
        "@media(min-width:900px)": {
            left: '-45px',
            top: "50px",
            position: 'relative',
            "& div": {
                position: 'absolute',
                top: '50%',
                '-ms-transform': 'translateY(-50%)',
                transform: 'translateY(-50%)',
                paddingLeft: '40px',
                paddingRight: '40px'
            }
        },
        "@media(max-width:900px)":{
            paddingTop:'75px',
            paddingBottom:'50px',
            top:'-25px',
            zIndex:'-1'
        }
    })
    return (
        <Container className={`position-relative ${ucapan}`}>
            <Row>
                <Col xs={12} md={6} className={`${shadow} ${left} p-0`}>
                    <img src={props.image} className="w-100 text-center" />
                </Col>
                <Col xs={12} md={6} className={`${shadow} ${right}`}>
                    <div>
                        {props.h2}
                        {props.h1}
                        {props.p}
                    </div>
                </Col>
            </Row>
        </Container>
    )
}
