import { Container, Row, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { css } from 'glamor'
export const BgHeader = (props) => {

    const header = css({
        backgroundPosition: 'center top',
        backgroundImage: `url('${props.background}')`,
        backgroundRepeat: `no - repeat`,
        backgroundSize: `cover`,
        height: `100vh`,
        "@media(max-width:900px)":{
            backgroundPosition: 'center',
        },
        '& .row': {
            color: '#D0B577'
        },
        '& p': {
            fontSize: '0.9em',
            width: '100%',
            textAlign: 'center',
            fontFamily: 'Quicksand, sans-serif',
            marginBottom: 0
        },
        '& h1': {
            fontFamily: 'photograph_signatureregular',
            fontWeight: 400,
            fontSize: '3.3em'
        },
        '& i': {
            letterSpacing: '-2px',
        },
        '& button.btn-light': {
            backgroundColor: '#D0B577',
            borderColor: '#D0B577',
            borderRadius: '15px',
            paddingLeft: '30px',
            paddingRight: '30px'
        },
        '& button p': {
            color: 'white'
        }
    })

    return (
        <Container fluid className={`${header} position-relative`}>
            <div/>
            <style global jsx>{`
        @import url('https://fonts.googleapis.com/css2?family=Quicksand&display=swap');
        @font-face {
            font-family: 'photograph_signatureregular';
            src: url('/font/photograph-signature-webfont.woff2') format('woff2'),
                 url('/font/photograph-signature-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @import url('https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap');
        @font-face {
            font-family: 'julietta_messieregular';
            src: url('/font/julietta-messie-webfont.woff2') format('woff2'),
                 url('/font/julietta-messie-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        
        }
          `}</style>
            <Row
                className="w-100 justify-content-center"
                style={{
                    position: 'absolute',
                    bottom: '10%'
                }}>
                {props.children}
            </Row>
        </Container>
    )

}
