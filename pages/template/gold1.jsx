import React, { Component } from 'react'
import { BgHeader, IsiHeader, Logo } from 'template/general/Header'
import { Ucapan } from 'template/general/Ucapan'
import { Button } from 'react-bootstrap'
import axios from "axios"

let id = 'mella-cahyo'
let lengkap_co = (<>Cahyo Rizki Dwiokta,ST  </>)
let lengkap_ce = (<>Wahyu Mella Harita,ST </>)

let bapak_co = 'Bpk Sudrajat'
let ibu_co = 'Ibu Sri Hartuti'
let bapak_ce = "Bpk Chamdi Aryandi"
let ibu_ce = "Ibu Sri Sunarsih"

let ig_co = "cahyo_rizki"
let ig_ce = "mellaharita"
export default class Gold1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            loading: true
        }
    }
    componentDidMount() {
        axios
            .get(`https://cors-anywhere.herokuapp.com/https://gitlab.com/arief15/data/-/raw/master/${id}.json`)
            .then(({ data }) => {
                console.log(data);
                this.setState(
                    { data: data, loading: false }
                    //
                );
            })
            .catch((err) => { })
    }
    render() {
        let { data, loading } = this.state
        return (
            <>
                {!loading ?
                    <>
                        <BgHeader background="https://di.undang.in/amel-danar/modal.jpg">
                            <p>{!data.title ? "THE WEDDING OF :" : ""}</p>
                            <h1>{data.inisial_ce} & {data.inisial_co}</h1>
                            <p>
                                <i className="pr-2">------------</i>
                                {data.tanggal}
                                <i className="pl-2">------------</i>
                            </p>
                            <Button variant="light mt-3">
                                <p>Open Invitiation</p>
                            </Button>
                        </BgHeader>
                        <Ucapan
                            image={data.gambar_awal}
                            h2={<h2>{data.text1}</h2>}
                            h1={<h1>{data.text2}</h1>}
                            p={
                                <p>
                                    {data.text3}
                                    <br />
                                    <br />
                                    {data.text4}
                                </p>
                            }
                        />

                    </>
                    : false
                }
            </>
        )
    }

}